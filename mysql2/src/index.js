const express = require ('express')
const {Sequelize, DataTypes, Model} = require ('sequelize')
const app = express()

//llamo al archivo de configuracion de la conexion de la base de datos
const {config} = require('./config/index')

//preparo la conexion pasando todos los parametros desde el archivo
const sequelize = new Sequelize (config.database, config.username, config.password, {
    host: config.host,
    dialect: config.dialect,
});

/*preparo el modelo segun la tabla que ya tengo en la base de datos. Es la misma del trabajo anterior
pero le cambiel nombre de bandas a artistas*/
const artistasModel = sequelize.define('artistas', {
    "id": {type:Sequelize.INTEGER, primaryKey:true},
    "nombre":{type: Sequelize.STRING},
    "integrantes": Sequelize.INTEGER,
    "fecha_inicio": Sequelize.DATE,
    "fecha_fin": Sequelize.DATE,
    "pais": Sequelize.STRING
})

//inicio la conexion
sequelize.authenticate()
    .then(()=>{
        console.log('CONEXIÓN A LA BASE DE DATOS OK')
    })
    .catch(error =>{
        console.log('EL ERROR DE CONEXION ES:'+error)
    })

//tomo de la base de datos los campos nombre y pais de la tabla artistas.
artistasModel.findAll({attributes:['nombre','pais']})
    .then(artistas =>{
        //el resultado que en este caso viene en la variable "artistas" lo paso por JSON.stringfy para que quede mejor visualmente y lo guardo en "resultados"
        const resultados = JSON.stringify(artistas)
        console.log(resultados);
    })
    .catch(error =>{
        console.log('EL ERROR DE CONEXION ES:'+error)
    })

app.listen(3000, ()=>{
    console.log('SERVER UP EN http://localhost:3000')
})

/*module.exports={sequelize}*/

