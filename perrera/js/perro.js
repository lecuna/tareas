class Perro {
    constructor(nombre, tamanio, edad, color, estadoAdopcion){
        this.nombre = nombre;
        this.tamanio = tamanio;
        this.edad = edad;
        this.color = color;
        this.estadoAdopcion =estadoAdopcion;
    }

    getNombre(){return this.nombre;}
    setNombre(nombre){this.nombre=nombre;}

    getTamanio(){return this.tamanio;}
    setTamanio(tamanio){this.tamanio=tamanio;}

    getEdad(){return this.edad;}
    setEdad(edad){this.edad=edad;}

    getColor(){return this.color;}
    setColor(color){this.color=color;}

    getEstadoAdopcion(){return this.estadoAdopcion;}
	setEstadoAdopcion(estado){this.estadoAdopcion = estado;}
}