let estado=["En adopcion","Proceso de adopció","Adoptado"];
let perro1 = new Perro("Helga", 55,8,"negro",estado[0]);
let perro2 = new Perro("Holga", 50,10,"gris",estado[1]);
let perro3 = new Perro("Chicho", 30,1,"blanco",estado[0]);

let perros = [];
perros.push(perro1,perro2,perro3);
listarPerros();

function cargarPerro(){
    let carga = true;
    while(carga){
        let nombre = prompt("Por favor ingrese nombre:");
        while (!nombre) {
            nombre = prompt("El campo nombre NO PUEDE estar vacio: ");
        }
    
        let tamanio = prompt("Por favor ingrese el tamaño en cm: ");
        while (!tamanio) {
            tamanio = prompt("El campo tamaño NO PUEDE estar vacio: ");
        }
    
        let edad = prompt("Edad aproximada: ");
        while (!edad) {
            edad = prompt("El campo edad NO PUEDE estar vacio: ");
        }
    
        let color = prompt("¿De que color es el perro?");
        while (!color) {
            color = prompt("El campo color NO PUEDE estar vacio: ");
        }
    
        let perroIngresado = new Perro(nombre, tamanio, edad, color, estado[0]);
        perros.push(perroIngresado);
        let respuestacorrecta = false;
    
        
        carga = window.confirm("¿Desea cargar otro perro?");S
    }
}

function verificarExistenciaID(idPerro)
{
    if (idPerro < perros.length && idPerro>=0)
    {
        return true;
    }
    else{
        return false;
    }
}
function verificarEstado(nuevoEstado){
    if (nuevoEstado < 3 && nuevoEstado>=0)
    {
        return true;
    }
    else{
        return false;
    }
}
function modificarEstado(){

    let idPerro = prompt("Por favor ingrese el ID del perro:");
     while (!verificarExistenciaID(idPerro)) {
        idPerro = prompt("El ID ingresado no existe: ");
    }

    let nuevoEstado = prompt("Por favor seleccione una opcion: \n1-En Adopcion\n2-Proceso de Adopcion\n3-Adoptado");
    while (!verificarEstado(nuevoEstado-1)) {
        nuevoEstado = prompt("La opcion seleccionada no existe, por favor seleccione una de la lista:\n1-En Adopcion\n2-Proceso de Adopcion\n3-Adoptado");
    }
    perros[idPerro].setEstadoAdopcion(estado[nuevoEstado-1]);
    listarPerros()
}

function listarPerros(){
    let codigoHtml = "<h1>Listado de perros</h1><table align=\"center\"><tr><th>ID</th><th>Nombre</th><th>Tamaño (cm)</th><th>Edad</th><th>Color</th><th>Estado</th></tr>";

    perros.forEach(function(perro, perroid){
        codigoHtml += "<tr><td>"+perroid+"</td>"+"<td>"+perro.getNombre()+"</td>"+"<td>"+perro.getTamanio()+" cm</td>"+"<td>"+perro.getEdad()+" años</td>"+"<td>"+perro.getColor()+"</td>"+"<td>"+perro.getEstadoAdopcion()+"</td></tr>";
    })
    codigoHtml += "</table>";
    codigoBotonHtml = "<input type=\"button\" class=\"button  button1\" onclick=\"cargarPerro();\" value=\"Carga de Perros\"></input><input type=\"button\" class=\"button  button1\" onclick=\"modificarEstado();\" value=\"Modificar Estado de Adopción\"></input>";
    codigoHtml += codigoBotonHtml;
    document.getElementById("listaPerros").innerHTML = codigoHtml;
}




S

