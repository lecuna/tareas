/*npm i fast-csv*/
/*npm i fs*/
/* PARA CREAR LA BASE DE DATOS Y SU USUARIO PARTICULAR
- mysql -u root -p (y nos pide la contraseña root)
- drop database personabulk; (verifico que no exista la base de datos)
- create database personabulk; (Creo la base de datos)
- create user personabulk@localhost identified by 'personabulk'; (genero el usuario. lo que esta despues de "identified by" es la contraseña)
- grant all privileges on personabulk.* to personabulk@localhost; (Le doy todos los permisos sobre la base de datos a este usuario)
*/
const fs = require("fs");

//const express = require ('express')
const {Sequelize, DataTypes, Model} = require ('sequelize')
//const app = express()

//llamo al archivo de configuracion de la conexion de la base de datos
const {config} = require('./config/index')

//preparo la conexion pasando todos los parametros desde el archivo
const sequelize = new Sequelize (config.database, config.username, config.password, {
    host: config.host,
    dialect: config.dialect,
});

//Tomo los datos del archivo CSV y lo guardo en la variable datos
let datos = fs.readFileSync('./data.csv', 'utf8' , (err, data) => {
    if (err) {
        console.error(err)
        return
    }
    })

//El primer parametro es lo que extraemos del archivo CSV
const CSVToArray = (data, delimiter = ',', omitFirstRow = false) =>
data
    .slice(omitFirstRow ? data.indexOf('\n') + 1 : 0)
    .split('\n')
    .map(v => v.split(delimiter));


//por alguna razon en los saltos de linea me queda guardado como valo  \r asique recorro el ultimo valor de cada fila y le borro esos caracteres
function quitarSalto(array, i){
    let newArray=[];
    array.forEach(element => {
        element[i]= element[i].toString().replace(/\r/,"");
        newArray.push(element);
    });
    console.log(newArray);
    return newArray;
}

//Junto todas las funciones anteriores quitar salto de linea -- CSVToArraY -- leer CSV.
arrayDatos = quitarSalto(CSVToArray(datos, ',', true),2)

//Preparo la primer parte del sql INSERT
let sql = "INSERT INTO `personas` (nombre, apellido, edad) VALUES "

//continuo armando el SQL pasando los valores
arrayDatos.forEach(function (fila){
    sql += `('${fila[0]}','${fila[1]}','${fila[2]}'),`
})

//como al final del armado del SQL me queda una coma y tira error. le borro el ultimo caracter al string
sql = sql.slice(0,-1);

//ejecuto la Query
sequelize.query(sql, { type: sequelize.QueryTypes.INSERT})
  


   